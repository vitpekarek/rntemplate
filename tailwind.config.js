import COLORS from './src/theme/colors';

module.exports = {
    theme: {
        extend: {
            // spacing: {
            //     128: '32rem',
            //     144: '36rem',
            // },
            // borderRadius: {
            //     '4xl': '2rem',
            // },
            colors: {
                'theme-white': COLORS.white,
                'theme-black': COLORS.black,
                'theme-green': COLORS.green,
                'theme-red': COLORS.red,
                'theme-blue': COLORS.blue,
                'theme-yellow': COLORS.yellow,
                dark: COLORS.dark,
                light: COLORS.light,
                primary: COLORS.primary,
                secondary: COLORS.secondary,
                'theme-light': COLORS.light,
                'theme-medium': COLORS.medium,
                'theme-dark': COLORS.dark,
            },
        },
    },
};

const COLORS = {
    primary: '#000',
    secondary: '#999',

    white: '#FFFFFF',
    black: '#000000',
    green: '#00AC76',
    red: '#C04345',
    blue: '#0043F9',
    yellow: '#F4EC44',

    light: '#F0F0F3',
    // backgroundMedium: '#B9B9B9',
    // backgroundDark: '#777777',
    medium: '#333',
    dark: '#1E1E20',
};

export default COLORS;

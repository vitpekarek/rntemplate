import React, {useEffect} from 'react';
import styled, {css} from '@emotion/native';
import tw from '../../common/tailwind';
import COLORS from '../../theme/colors';
import {useSelector, useDispatch} from 'react-redux';
import {decrement, increment} from '../../reducers/mainReducer';
import {
    ScrollView,
    StatusBar,
    TouchableOpacity,
    Text,
    View,
    Button,
    Image,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const HomeScreen = ({navigation}) => {
    // const products = useSelector(state => state.shop.products);
    const dispatch = useDispatch();

    // useEffect(() => {
    //     const unsubscribe = navigation.addListener('focus', () => {
    //         dispatch(setProducts());
    //     });
    //
    //     return unsubscribe;
    // }, [navigation, dispatch]);

    return (
        <View style={tw`h-[100%] w-[100%] bg-light`}>
            <StatusBar
                backgroundColor={COLORS.light}
                barStyle="dark-content"
            />
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={tw`w-[100%] flex flex-row justify-between p-3`}>
                    <TouchableOpacity>
                        <Entypo
                            name="shopping-bag"
                            style={tw`text-lg leading-none text-theme-dark p-3 rounded-md bg-theme-light`}
                        />
                    </TouchableOpacity>
                    {/*<Image
                        source={require('../../assets/images/logo.png')}
                        style={tw`w-[150px] h-[100px]`}
                    />*/}
                    <TouchableOpacity
                        onPress={() => navigation.navigate('MyCart')}>
                        <MaterialCommunityIcons
                            name="cart"
                            style={tw`text-lg leading-none text-theme-dark p-3 rounded-md border border-theme-medium`}
                        />
                    </TouchableOpacity>
                </View>
                <View style={tw`mb-2 p-3 items-center justify-center`}>
                    <Text
                        style={tw`text-sm text-theme-dark font-normal tracking-wider opacity-50`}>
                        text
                    </Text>
                </View>
                <View style={tw`p-4`}>
                    <View style={tw`flex-row items-center justify-between`}>
                        <View style={tw`flex-row items-center`}>
                            <Text
                                style={tw`text-lg text-theme-dark font-medium tracking-wide`}>
                                text1
                            </Text>
                            <Text
                                style={tw`text-sm text-theme-dark opacity-50 font-normal tracking-wide ml-2`}>
                                10
                            </Text>
                        </View>
                        <Text style={tw`text-xs text-primary font-medium`}>
                            test2
                        </Text>
                    </View>
                    <View style={tw`flex-row flex-wrap justify-around`}></View>
                </View>
            </ScrollView>
        </View>
    );
};

export default HomeScreen;
